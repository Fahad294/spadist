export default [
  {
    itemTitle: 'Custom Requirements Tool',
    itemSubtitle: 'Find import & export laws for each country.',
    itemImage: '/statics/icons/icon-05.png',
    itemPageUrl: '/customs-needs',
  },
  {
    itemTitle: 'Health Requirement Tool',
    itemSubtitle: 'Dont take risk check out the requirements for each country.',
    itemImage: '/statics/icons/icon-01.png',
    itemPageUrl: '/health-requirements',
  },
  /*
  {
    itemTitle: 'Pets Requirements Tool',
    itemSubtitle: 'Check the countries requirements when traveling with Pets.',
    itemImage: '/statics/icons/icon-03.png',
    itemPageUrl: '/tools/pets_requirements',
  },*/
  {
    itemTitle: 'Cheap Flight Finder',
    itemSubtitle: 'Search cheap flights worldwide to save money for other activities.',
    itemImage: '/statics/icons/icon-11.png',
    itemPageUrl: 'https://flights.visadb.io',
    out: true
  },
  {
    itemTitle: 'Cheap Hotel Finder',
    itemSubtitle: 'Search and book cheap hotels to make the most out of holidays.',
    itemImage: '/statics/icons/icon-12.png',
    itemPageUrl: 'https://flights.visadb.io/hotels',
    out: true
  },
  {
    itemTitle: 'Citizenship Tool',
    itemSubtitle: 'Citizenship tool will find routes to get you 2nd Citizenship.',
    itemImage: '/statics/icons/icon-02.png',
    itemPageUrl: '/tools/citizenship_finder_new',
  },
  /*
  {
    itemTitle: 'Scholarship Finder',
    itemSubtitle: 'Find Scholarships around the world to get free quality education & experience.',
    itemImage: '/statics/icons/icon-22.jpg',
    itemPageUrl: '/tools/scholarship_finder',
  },*/
];
