const pages= {
	citizenshiptool: [
		{
			q: "What is our Citizenship Tool?",
			a: "Our tool pulls up all possible 2nd Citizenship routes based on the destination country’s visas and shows the timeline of permanent residency and citizenship based on the visa selected."
		},
		{
			q: "How many countries are covered in this tool?",
			a: "We cover 150+ countries in this tool."
		},
		{
			q: "How is the data monitored and updated?",
			a: "We monitor government immigration and foreign affairs websites to keep the information real time."
		},
		{
			q: "What is the validity of this service?",
			a: "This paid service has lifetime access and will remain effect for life."
		},
		{
			q: "Why do I need to pay for the lifetime access service?",
			a: "The tool is constantly maintained real-time and requires a lot of resources to do that. This payment acts as a contribution to this business model"
		},
		{
			q: "Is the lifetime premium access service refundable?",
			a: "No.The lifetime premium access service is a one-time payment and is not refundable."
		}
	],
	passport:[
		{
			q: "What is the Global Passport Ranking tool?",
			a: "Our tool shows the ranking of passports for over 200 countries, based on how a passport is seen by other sovereign governments along with efforts and actions required to enter the land."
		},
		{
			q: "What is the scoring methodology?",
			a: "Our strength scoring system is mainly score on how a passport is seen by other sovereign governments along with efforts and actions required to enter the land. Thus, each passport gets points (per country) based on the entry requirements:  Visa-free access gets 2 points, Visa on Arrival (VOA) - 1.5 points, ETA/Electronic – 1 points, Visa required – 0 points, Banned access gets (-1) point. We sum up passport points for 200 countries and rank based on total points."
		},
		{
			q: "How many countries are covered in this tool?",
			a: "We list 200 countries (195 UN recognised + Israel, Palestine, Tuvulu)"
		},
		{
			q: "How is the data monitored and updated?",
			a: "We monitor immigration departments and government agencies for 200 countries for real-time updates."
		}
	],
}

export default pages;