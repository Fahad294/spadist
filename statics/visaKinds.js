export default [
  {
    title: 'Visit Visa',
    count: 254,
  },
  {
    title: 'Startup visa',
    count: 240,
  },
  {
    title: 'Work visas',
    count: 240,
  },
  {
    title: 'Residence visas',
    count: 240,
  },
  {
    title: 'Immigrant visas',
    count: 240,
  },
  {
    title: 'Working holidays visas',
    count: 65,
  },
];

