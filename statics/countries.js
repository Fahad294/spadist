import countriesSelect from './countriesSelect.json';
import continents from './continents.js';

const countries = [
  {
    "avatar": "/statics/flags/AF.png",
    "continent": false,
    "ind": "AF",
    "inset": true,
    "label": "Afghanistan",
    "value": 0
  },
  {
    "avatar": "/statics/flags/AX.png",
    "continent": false,
    "ind": "AX",
    "inset": true,
    "label": "Åland Islands",
    "value": 51
  },
  {
    "avatar": "/statics/flags/AL.png",
    "continent": false,
    "ind": "AL",
    "inset": true,
    "label": "Albania",
    "value": 52
  },
  {
    "avatar": "/statics/flags/DZ.png",
    "continent": false,
    "ind": "DZ",
    "inset": true,
    "label": "Algeria",
    "value": 102
  },
  {
    "avatar": "/statics/flags/AS.png",
    "continent": false,
    "ind": "AS",
    "inset": true,
    "label": "American Samoa",
    "value": 215
  },
  {
    "avatar": "/statics/flags/AD.png",
    "continent": false,
    "ind": "AD",
    "inset": true,
    "label": "Andorra",
    "value": 53
  },
  {
    "avatar": "/statics/flags/AO.png",
    "continent": false,
    "ind": "AO",
    "inset": true,
    "label": "Angola",
    "value": 103
  },
  {
    "avatar": "/statics/flags/AI.png",
    "continent": false,
    "ind": "AI",
    "inset": true,
    "label": "Anguilla",
    "value": 160
  },
  {
    "avatar": "/statics/flags/AG.png",
    "continent": false,
    "ind": "AG",
    "inset": true,
    "label": "Antigua and Barbuda",
    "value": 161
  },
  {
    "avatar": "/statics/flags/AR.png",
    "continent": false,
    "ind": "AR",
    "inset": true,
    "label": "Argentina",
    "value": 162
  },
  {
    "avatar": "/statics/flags/AM.png",
    "continent": false,
    "ind": "AM",
    "inset": true,
    "label": "Armenia",
    "value": 1
  },
  {
    "avatar": "/statics/flags/AW.png",
    "continent": false,
    "ind": "AW",
    "inset": true,
    "label": "Aruba",
    "value": 163
  },
  {
    "avatar": "/statics/flags/AU.png",
    "continent": false,
    "ind": "AU",
    "inset": true,
    "label": "Australia",
    "value": 216
  },
  {
    "avatar": "/statics/flags/AT.png",
    "continent": false,
    "ind": "AT",
    "inset": true,
    "label": "Austria",
    "value": 54
  },
  {
    "avatar": "/statics/flags/AZ.png",
    "continent": false,
    "ind": "AZ",
    "inset": true,
    "label": "Azerbaijan",
    "value": 2
  },
  {
    "avatar": "/statics/flags/BS.png",
    "continent": false,
    "ind": "BS",
    "inset": true,
    "label": "Bahamas",
    "value": 164
  },
  {
    "avatar": "/statics/flags/BH.png",
    "continent": false,
    "ind": "BH",
    "inset": true,
    "label": "Bahrain",
    "value": 3
  },
  {
    "avatar": "/statics/flags/BD.png",
    "continent": false,
    "ind": "BD",
    "inset": true,
    "label": "Bangladesh",
    "value": 4
  },
  {
    "avatar": "/statics/flags/BB.png",
    "continent": false,
    "ind": "BB",
    "inset": true,
    "label": "Barbados",
    "value": 165
  },
  {
    "avatar": "/statics/flags/BY.png",
    "continent": false,
    "ind": "BY",
    "inset": true,
    "label": "Belarus",
    "value": 55
  },
  {
    "avatar": "/statics/flags/BE.png",
    "continent": false,
    "ind": "BE",
    "inset": true,
    "label": "Belgium",
    "value": 56
  },
  {
    "avatar": "/statics/flags/BZ.png",
    "continent": false,
    "ind": "BZ",
    "inset": true,
    "label": "Belize",
    "value": 166
  },
  {
    "avatar": "/statics/flags/BJ.png",
    "continent": false,
    "ind": "BJ",
    "inset": true,
    "label": "Benin",
    "value": 104
  },
  {
    "avatar": "/statics/flags/BM.png",
    "continent": false,
    "ind": "BM",
    "inset": true,
    "label": "Bermuda",
    "value": 167
  },
  {
    "avatar": "/statics/flags/BT.png",
    "continent": false,
    "ind": "BT",
    "inset": true,
    "label": "Bhutan",
    "value": 5
  },
  {
    "avatar": "/statics/flags/BO.png",
    "continent": false,
    "ind": "BO",
    "inset": true,
    "label": "Bolivia",
    "value": 168
  },
  {
    "avatar": "/statics/flags/BQ.png",
    "continent": false,
    "ind": "BQ",
    "inset": true,
    "label": "Bonaire",
    "value": 169
  },
  {
    "avatar": "/statics/flags/BA.png",
    "continent": false,
    "ind": "BA",
    "inset": true,
    "label": "Bosnia and Herzegovina",
    "value": 57
  },
  {
    "avatar": "/statics/flags/BW.png",
    "continent": false,
    "ind": "BW",
    "inset": true,
    "label": "Botswana",
    "value": 105
  },
  {
    "avatar": "/statics/flags/BR.png",
    "continent": false,
    "ind": "BR",
    "inset": true,
    "label": "Brazil",
    "value": 170
  },
  {
    "avatar": "/statics/flags/BN.png",
    "continent": false,
    "ind": "BN",
    "inset": true,
    "label": "Brunei Darussalam",
    "value": 6
  },
  {
    "avatar": "/statics/flags/BG.png",
    "continent": false,
    "ind": "BG",
    "inset": true,
    "label": "Bulgaria",
    "value": 58
  },
  {
    "avatar": "/statics/flags/BF.png",
    "continent": false,
    "ind": "BF",
    "inset": true,
    "label": "Burkina Faso",
    "value": 106
  },
  {
    "avatar": "/statics/flags/BI.png",
    "continent": false,
    "ind": "BI",
    "inset": true,
    "label": "Burundi",
    "value": 107
  },
  {
    "avatar": "/statics/flags/CV.png",
    "continent": false,
    "ind": "CV",
    "inset": true,
    "label": "Cabo Verde",
    "value": 109
  },
  {
    "avatar": "/statics/flags/KH.png",
    "continent": false,
    "ind": "KH",
    "inset": true,
    "label": "Cambodia",
    "value": 7
  },
  {
    "avatar": "/statics/flags/CM.png",
    "continent": false,
    "ind": "CM",
    "inset": true,
    "label": "Cameroon",
    "value": 108
  },
  {
    "avatar": "/statics/flags/CA.png",
    "continent": false,
    "ind": "CA",
    "inset": true,
    "label": "Canada",
    "value": 171
  },
  {
    "avatar": "/statics/flags/KY.png",
    "continent": false,
    "ind": "KY",
    "inset": true,
    "label": "Cayman Islands",
    "value": 172
  },
  {
    "avatar": "/statics/flags/CF.png",
    "continent": false,
    "ind": "CF",
    "inset": true,
    "label": "Central African Republic",
    "value": 110
  },
  {
    "avatar": "/statics/flags/TD.png",
    "continent": false,
    "ind": "TD",
    "inset": true,
    "label": "Chad",
    "value": 111
  },
  {
    "avatar": "/statics/flags/CL.png",
    "continent": false,
    "ind": "CL",
    "inset": true,
    "label": "Chile",
    "value": 173
  },
  {
    "avatar": "/statics/flags/CN.png",
    "continent": false,
    "ind": "CN",
    "inset": true,
    "label": "China",
    "value": 8
  },
  {
    "avatar": "/statics/flags/CO.png",
    "continent": false,
    "ind": "CO",
    "inset": true,
    "label": "Colombia",
    "value": 174
  },
  {
    "avatar": "/statics/flags/KM.png",
    "continent": false,
    "ind": "KM",
    "inset": true,
    "label": "Comoros",
    "value": 112
  },
  {
    "avatar": "/statics/flags/CG.png",
    "continent": false,
    "ind": "CG",
    "inset": true,
    "label": "Congo",
    "value": 113
  },
  {
    "avatar": "/statics/flags/CD.png",
    "continent": false,
    "ind": "CD",
    "inset": true,
    "label": "Congo",
    "value": 114
  },
  {
    "avatar": "/statics/flags/CK.png",
    "continent": false,
    "ind": "CK",
    "inset": true,
    "label": "Cook Islands",
    "value": 217
  },
  {
    "avatar": "/statics/flags/CR.png",
    "continent": false,
    "ind": "CR",
    "inset": true,
    "label": "Costa Rica",
    "value": 175
  },
  {
    "avatar": "/statics/flags/CI.png",
    "continent": false,
    "ind": "CI",
    "inset": true,
    "label": "Côte d'Ivoire",
    "value": 115
  },
  {
    "avatar": "/statics/flags/HR.png",
    "continent": false,
    "ind": "HR",
    "inset": true,
    "label": "Croatia",
    "value": 59
  },
  {
    "avatar": "/statics/flags/CU.png",
    "continent": false,
    "ind": "CU",
    "inset": true,
    "label": "Cuba",
    "value": 176
  },
  {
    "avatar": "/statics/flags/CW.png",
    "continent": false,
    "ind": "CW",
    "inset": true,
    "label": "Curaçao",
    "value": 177
  },
  {
    "avatar": "/statics/flags/CY.png",
    "continent": false,
    "ind": "CY",
    "inset": true,
    "label": "Cyprus",
    "value": 9
  },
  {
    "avatar": "/statics/flags/CZ.png",
    "continent": false,
    "ind": "CZ",
    "inset": true,
    "label": "Czech Republic",
    "value": 60
  },
  {
    "avatar": "/statics/flags/DK.png",
    "continent": false,
    "ind": "DK",
    "inset": true,
    "label": "Denmark",
    "value": 61
  },
  {
    "avatar": "/statics/flags/DJ.png",
    "continent": false,
    "ind": "DJ",
    "inset": true,
    "label": "Djibouti",
    "value": 116
  },
  {
    "avatar": "/statics/flags/DM.png",
    "continent": false,
    "ind": "DM",
    "inset": true,
    "label": "Dominica",
    "value": 178
  },
  {
    "avatar": "/statics/flags/DO.png",
    "continent": false,
    "ind": "DO",
    "inset": true,
    "label": "Dominican Republic",
    "value": 179
  },
  {
    "avatar": "/statics/flags/EC.png",
    "continent": false,
    "ind": "EC",
    "inset": true,
    "label": "Ecuador",
    "value": 180
  },
  {
    "avatar": "/statics/flags/EG.png",
    "continent": false,
    "ind": "EG",
    "inset": true,
    "label": "Egypt",
    "value": 117
  },
  {
    "avatar": "/statics/flags/SV.png",
    "continent": false,
    "ind": "SV",
    "inset": true,
    "label": "El Salvador",
    "value": 181
  },
  {
    "avatar": "/statics/flags/GQ.png",
    "continent": false,
    "ind": "GQ",
    "inset": true,
    "label": "Equatorial Guinea",
    "value": 118
  },
  {
    "avatar": "/statics/flags/ER.png",
    "continent": false,
    "ind": "ER",
    "inset": true,
    "label": "Eritrea",
    "value": 119
  },
  {
    "avatar": "/statics/flags/EE.png",
    "continent": false,
    "ind": "EE",
    "inset": true,
    "label": "Estonia",
    "value": 62
  },
  {
    "avatar": "/statics/flags/ET.png",
    "continent": false,
    "ind": "ET",
    "inset": true,
    "label": "Ethiopia",
    "value": 120
  },
  {
    "avatar": "/statics/flags/FK.png",
    "continent": false,
    "ind": "FK",
    "inset": true,
    "label": "Falkland Islands",
    "value": 182
  },
  {
    "avatar": "/statics/flags/FO.png",
    "continent": false,
    "ind": "FO",
    "inset": true,
    "label": "Faroe Islands",
    "value": 63
  },
  {
    "avatar": "/statics/flags/FJ.png",
    "continent": false,
    "ind": "FJ",
    "inset": true,
    "label": "Fiji",
    "value": 218
  },
  {
    "avatar": "/statics/flags/FI.png",
    "continent": false,
    "ind": "FI",
    "inset": true,
    "label": "Finland",
    "value": 64
  },
  {
    "avatar": "/statics/flags/FR.png",
    "continent": false,
    "ind": "FR",
    "inset": true,
    "label": "France",
    "value": 65
  },
  {
    "avatar": "/statics/flags/GF.png",
    "continent": false,
    "ind": "GF",
    "inset": true,
    "label": "French Guiana",
    "value": 183
  },
  {
    "avatar": "/statics/flags/PF.png",
    "continent": false,
    "ind": "PF",
    "inset": true,
    "label": "French Polynesia",
    "value": 219
  },
  {
    "avatar": "/statics/flags/GA.png",
    "continent": false,
    "ind": "GA",
    "inset": true,
    "label": "Gabon",
    "value": 121
  },
  {
    "avatar": "/statics/flags/GM.png",
    "continent": false,
    "ind": "GM",
    "inset": true,
    "label": "Gambia",
    "value": 122
  },
  {
    "avatar": "/statics/flags/GE.png",
    "continent": false,
    "ind": "GE",
    "inset": true,
    "label": "Georgia",
    "value": 10
  },
  {
    "avatar": "/statics/flags/DE.png",
    "continent": false,
    "ind": "DE",
    "inset": true,
    "label": "Germany",
    "value": 66
  },
  {
    "avatar": "/statics/flags/GH.png",
    "continent": false,
    "ind": "GH",
    "inset": true,
    "label": "Ghana",
    "value": 123
  },
  {
    "avatar": "/statics/flags/GI.png",
    "continent": false,
    "ind": "GI",
    "inset": true,
    "label": "Gibraltar",
    "value": 67
  },
  {
    "avatar": "/statics/flags/GR.png",
    "continent": false,
    "ind": "GR",
    "inset": true,
    "label": "Greece",
    "value": 68
  },
  {
    "avatar": "/statics/flags/GL.png",
    "continent": false,
    "ind": "GL",
    "inset": true,
    "label": "Greenland",
    "value": 184
  },
  {
    "avatar": "/statics/flags/GD.png",
    "continent": false,
    "ind": "GD",
    "inset": true,
    "label": "Grenada",
    "value": 185
  },
  {
    "avatar": "/statics/flags/GP.png",
    "continent": false,
    "ind": "GP",
    "inset": true,
    "label": "Guadeloupe",
    "value": 186
  },
  {
    "avatar": "/statics/flags/GU.png",
    "continent": false,
    "ind": "GU",
    "inset": true,
    "label": "Guam",
    "value": 220
  },
  {
    "avatar": "/statics/flags/GT.png",
    "continent": false,
    "ind": "GT",
    "inset": true,
    "label": "Guatemala",
    "value": 187
  },
  {
    "avatar": "/statics/flags/GG.png",
    "continent": false,
    "ind": "GG",
    "inset": true,
    "label": "Guernsey",
    "value": 69
  },
  {
    "avatar": "/statics/flags/GN.png",
    "continent": false,
    "ind": "GN",
    "inset": true,
    "label": "Guinea",
    "value": 124
  },
  {
    "avatar": "/statics/flags/GW.png",
    "continent": false,
    "ind": "GW",
    "inset": true,
    "label": "Guinea-Bissau",
    "value": 125
  },
  {
    "avatar": "/statics/flags/GY.png",
    "continent": false,
    "ind": "GY",
    "inset": true,
    "label": "Guyana",
    "value": 188
  },
  {
    "avatar": "/statics/flags/HT.png",
    "continent": false,
    "ind": "HT",
    "inset": true,
    "label": "Haiti",
    "value": 189
  },
  {
    "avatar": "/statics/flags/HN.png",
    "continent": false,
    "ind": "HN",
    "inset": true,
    "label": "Honduras",
    "value": 190
  },
  {
    "avatar": "/statics/flags/HK.png",
    "continent": false,
    "ind": "HK",
    "inset": true,
    "label": "Hong Kong",
    "value": 11
  },
  {
    "avatar": "/statics/flags/HU.png",
    "continent": false,
    "ind": "HU",
    "inset": true,
    "label": "Hungary",
    "value": 71
  },
  {
    "avatar": "/statics/flags/IS.png",
    "continent": false,
    "ind": "IS",
    "inset": true,
    "label": "Iceland",
    "value": 72
  },
  {
    "avatar": "/statics/flags/IN.png",
    "continent": false,
    "ind": "IN",
    "inset": true,
    "label": "India",
    "value": 12
  },
  {
    "avatar": "/statics/flags/ID.png",
    "continent": false,
    "ind": "ID",
    "inset": true,
    "label": "Indonesia",
    "value": 13
  },
  {
    "avatar": "/statics/flags/IR.png",
    "continent": false,
    "ind": "IR",
    "inset": true,
    "label": "Iran",
    "value": 14
  },
  {
    "avatar": "/statics/flags/IQ.png",
    "continent": false,
    "ind": "IQ",
    "inset": true,
    "label": "Iraq",
    "value": 15
  },
  {
    "avatar": "/statics/flags/IE.png",
    "continent": false,
    "ind": "IE",
    "inset": true,
    "label": "Ireland",
    "value": 73
  },
  {
    "avatar": "/statics/flags/IM.png",
    "continent": false,
    "ind": "IM",
    "inset": true,
    "label": "Isle of Man",
    "value": 74
  },
  {
    "avatar": "/statics/flags/IL.png",
    "continent": false,
    "ind": "IL",
    "inset": true,
    "label": "Israel",
    "value": 16
  },
  {
    "avatar": "/statics/flags/IT.png",
    "continent": false,
    "ind": "IT",
    "inset": true,
    "label": "Italy",
    "value": 75
  },
  {
    "avatar": "/statics/flags/JM.png",
    "continent": false,
    "ind": "JM",
    "inset": true,
    "label": "Jamaica",
    "value": 191
  },
  {
    "avatar": "/statics/flags/JP.png",
    "continent": false,
    "ind": "JP",
    "inset": true,
    "label": "Japan",
    "value": 17
  },
  {
    "avatar": "/statics/flags/JE.png",
    "continent": false,
    "ind": "JE",
    "inset": true,
    "label": "Jersey",
    "value": 76
  },
  {
    "avatar": "/statics/flags/JO.png",
    "continent": false,
    "ind": "JO",
    "inset": true,
    "label": "Jordan",
    "value": 18
  },
  {
    "avatar": "/statics/flags/KZ.png",
    "continent": false,
    "ind": "KZ",
    "inset": true,
    "label": "Kazakhstan",
    "value": 19
  },
  {
    "avatar": "/statics/flags/KE.png",
    "continent": false,
    "ind": "KE",
    "inset": true,
    "label": "Kenya",
    "value": 126
  },
  {
    "avatar": "/statics/flags/KI.png",
    "continent": false,
    "ind": "KI",
    "inset": true,
    "label": "Kiribati",
    "value": 221
  },
  {
    "avatar": "/statics/flags/KW.png",
    "continent": false,
    "ind": "KW",
    "inset": true,
    "label": "Kuwait",
    "value": 22
  },
  {
    "avatar": "/statics/flags/KG.png",
    "continent": false,
    "ind": "KG",
    "inset": true,
    "label": "Kyrgyzstan",
    "value": 23
  },
  {
    "avatar": "/statics/flags/LA.png",
    "continent": false,
    "ind": "LA",
    "inset": true,
    "label": "Laos",
    "value": 24
  },
  {
    "avatar": "/statics/flags/LV.png",
    "continent": false,
    "ind": "LV",
    "inset": true,
    "label": "Latvia",
    "value": 77
  },
  {
    "avatar": "/statics/flags/LB.png",
    "continent": false,
    "ind": "LB",
    "inset": true,
    "label": "Lebanon",
    "value": 25
  },
  {
    "avatar": "/statics/flags/LS.png",
    "continent": false,
    "ind": "LS",
    "inset": true,
    "label": "Lesotho",
    "value": 127
  },
  {
    "avatar": "/statics/flags/LR.png",
    "continent": false,
    "ind": "LR",
    "inset": true,
    "label": "Liberia",
    "value": 128
  },
  {
    "avatar": "/statics/flags/LY.png",
    "continent": false,
    "ind": "LY",
    "inset": true,
    "label": "Libya",
    "value": 129
  },
  {
    "avatar": "/statics/flags/LI.png",
    "continent": false,
    "ind": "LI",
    "inset": true,
    "label": "Liechtenstein",
    "value": 78
  },
  {
    "avatar": "/statics/flags/LT.png",
    "continent": false,
    "ind": "LT",
    "inset": true,
    "label": "Lithuania",
    "value": 79
  },
  {
    "avatar": "/statics/flags/LU.png",
    "continent": false,
    "ind": "LU",
    "inset": true,
    "label": "Luxembourg",
    "value": 80
  },
  {
    "avatar": "/statics/flags/MO.png",
    "continent": false,
    "ind": "MO",
    "inset": true,
    "label": "Macao",
    "value": 26
  },
  {
    "avatar": "/statics/flags/MK.png",
    "continent": false,
    "ind": "MK",
    "inset": true,
    "label": "Macedonia",
    "value": 81
  },
  {
    "avatar": "/statics/flags/MG.png",
    "continent": false,
    "ind": "MG",
    "inset": true,
    "label": "Madagascar",
    "value": 130
  },
  {
    "avatar": "/statics/flags/MW.png",
    "continent": false,
    "ind": "MW",
    "inset": true,
    "label": "Malawi",
    "value": 131
  },
  {
    "avatar": "/statics/flags/MY.png",
    "continent": false,
    "ind": "MY",
    "inset": true,
    "label": "Malaysia",
    "value": 27
  },
  {
    "avatar": "/statics/flags/MV.png",
    "continent": false,
    "ind": "MV",
    "inset": true,
    "label": "Maldives",
    "value": 28
  },
  {
    "avatar": "/statics/flags/ML.png",
    "continent": false,
    "ind": "ML",
    "inset": true,
    "label": "Mali",
    "value": 132
  },
  {
    "avatar": "/statics/flags/MT.png",
    "continent": false,
    "ind": "MT",
    "inset": true,
    "label": "Malta",
    "value": 82
  },
  {
    "avatar": "/statics/flags/MH.png",
    "continent": false,
    "ind": "MH",
    "inset": true,
    "label": "Marshall Islands",
    "value": 222
  },
  {
    "avatar": "/statics/flags/MQ.png",
    "continent": false,
    "ind": "MQ",
    "inset": true,
    "label": "Martinique",
    "value": 192
  },
  {
    "avatar": "/statics/flags/MR.png",
    "continent": false,
    "ind": "MR",
    "inset": true,
    "label": "Mauritania",
    "value": 133
  },
  {
    "avatar": "/statics/flags/MU.png",
    "continent": false,
    "ind": "MU",
    "inset": true,
    "label": "Mauritius",
    "value": 134
  },
  {
    "avatar": "/statics/flags/YT.png",
    "continent": false,
    "ind": "YT",
    "inset": true,
    "label": "Mayotte",
    "value": 135
  },
  {
    "avatar": "/statics/flags/MX.png",
    "continent": false,
    "ind": "MX",
    "inset": true,
    "label": "Mexico",
    "value": 193
  },
  {
    "avatar": "/statics/flags/FM.png",
    "continent": false,
    "ind": "FM",
    "inset": true,
    "label": "Micronesia",
    "value": 223
  },
  {
    "avatar": "/statics/flags/MD.png",
    "continent": false,
    "ind": "MD",
    "inset": true,
    "label": "Moldova",
    "value": 83
  },
  {
    "avatar": "/statics/flags/MC.png",
    "continent": false,
    "ind": "MC",
    "inset": true,
    "label": "Monaco",
    "value": 84
  },
  {
    "avatar": "/statics/flags/MN.png",
    "continent": false,
    "ind": "MN",
    "inset": true,
    "label": "Mongolia",
    "value": 29
  },
  {
    "avatar": "/statics/flags/ME.png",
    "continent": false,
    "ind": "ME",
    "inset": true,
    "label": "Montenegro",
    "value": 85
  },
  {
    "avatar": "/statics/flags/MS.png",
    "continent": false,
    "ind": "MS",
    "inset": true,
    "label": "Montserrat",
    "value": 194
  },
  {
    "avatar": "/statics/flags/MA.png",
    "continent": false,
    "ind": "MA",
    "inset": true,
    "label": "Morocco",
    "value": 136
  },
  {
    "avatar": "/statics/flags/MZ.png",
    "continent": false,
    "ind": "MZ",
    "inset": true,
    "label": "Mozambique",
    "value": 137
  },
  {
    "avatar": "/statics/flags/MM.png",
    "continent": false,
    "ind": "MM",
    "inset": true,
    "label": "Myanmar",
    "value": 30
  },
  {
    "avatar": "/statics/flags/NA.png",
    "continent": false,
    "ind": "NA",
    "inset": true,
    "label": "Namibia",
    "value": 138
  },
  {
    "avatar": "/statics/flags/NR.png",
    "continent": false,
    "ind": "NR",
    "inset": true,
    "label": "Nauru",
    "value": 224
  },
  {
    "avatar": "/statics/flags/NP.png",
    "continent": false,
    "ind": "NP",
    "inset": true,
    "label": "Nepal",
    "value": 31
  },
  {
    "avatar": "/statics/flags/NL.png",
    "continent": false,
    "ind": "NL",
    "inset": true,
    "label": "Netherlands",
    "value": 86
  },
  {
    "avatar": "/statics/flags/NC.png",
    "continent": false,
    "ind": "NC",
    "inset": true,
    "label": "New Caledonia",
    "value": 225
  },
  {
    "avatar": "/statics/flags/NZ.png",
    "continent": false,
    "ind": "NZ",
    "inset": true,
    "label": "New Zealand",
    "value": 226
  },
  {
    "avatar": "/statics/flags/NI.png",
    "continent": false,
    "ind": "NI",
    "inset": true,
    "label": "Nicaragua",
    "value": 195
  },
  {
    "avatar": "/statics/flags/NE.png",
    "continent": false,
    "ind": "NE",
    "inset": true,
    "label": "Niger",
    "value": 139
  },
  {
    "avatar": "/statics/flags/NG.png",
    "continent": false,
    "ind": "NG",
    "inset": true,
    "label": "Nigeria",
    "value": 140
  },
  {
    "avatar": "/statics/flags/NU.png",
    "continent": false,
    "ind": "NU",
    "inset": true,
    "label": "Niue",
    "value": 227
  },
  {
    "avatar": "/statics/flags/NF.png",
    "continent": false,
    "ind": "NF",
    "inset": true,
    "label": "Norfolk Island",
    "value": 228
  },
  {
    "avatar": "/statics/flags/KP.png",
    "continent": false,
    "ind": "KP",
    "inset": true,
    "label": "North Korea",
    "value": 20
  },
  {
    "avatar": "/statics/flags/MP.png",
    "continent": false,
    "ind": "MP",
    "inset": true,
    "label": "Northern Mariana Islands",
    "value": 229
  },
  {
    "avatar": "/statics/flags/NO.png",
    "continent": false,
    "ind": "NO",
    "inset": true,
    "label": "Norway",
    "value": 87
  },
  {
    "avatar": "/statics/flags/OM.png",
    "continent": false,
    "ind": "OM",
    "inset": true,
    "label": "Oman",
    "value": 32
  },
  {
    "avatar": "/statics/flags/PK.png",
    "continent": false,
    "ind": "PK",
    "inset": true,
    "label": "Pakistan",
    "value": 33
  },
  {
    "avatar": "/statics/flags/PW.png",
    "continent": false,
    "ind": "PW",
    "inset": true,
    "label": "Palau",
    "value": 230
  },
  {
    "avatar": "/statics/flags/PS.png",
    "continent": false,
    "ind": "PS",
    "inset": true,
    "label": "Palestine",
    "value": 34
  },
  {
    "avatar": "/statics/flags/PA.png",
    "continent": false,
    "ind": "PA",
    "inset": true,
    "label": "Panama",
    "value": 196
  },
  {
    "avatar": "/statics/flags/PG.png",
    "continent": false,
    "ind": "PG",
    "inset": true,
    "label": "Papua New Guinea",
    "value": 231
  },
  {
    "avatar": "/statics/flags/PY.png",
    "continent": false,
    "ind": "PY",
    "inset": true,
    "label": "Paraguay",
    "value": 197
  },
  {
    "avatar": "/statics/flags/PE.png",
    "continent": false,
    "ind": "PE",
    "inset": true,
    "label": "Peru",
    "value": 198
  },
  {
    "avatar": "/statics/flags/PH.png",
    "continent": false,
    "ind": "PH",
    "inset": true,
    "label": "Philippines",
    "value": 35
  },
  {
    "avatar": "/statics/flags/PN.png",
    "continent": false,
    "ind": "PN",
    "inset": true,
    "label": "Pitcairn",
    "value": 232
  },
  {
    "avatar": "/statics/flags/PL.png",
    "continent": false,
    "ind": "PL",
    "inset": true,
    "label": "Poland",
    "value": 88
  },
  {
    "avatar": "/statics/flags/PT.png",
    "continent": false,
    "ind": "PT",
    "inset": true,
    "label": "Portugal",
    "value": 89
  },
  {
    "avatar": "/statics/flags/PR.png",
    "continent": false,
    "ind": "PR",
    "inset": true,
    "label": "Puerto Rico",
    "value": 199
  },
  {
    "avatar": "/statics/flags/QA.png",
    "continent": false,
    "ind": "QA",
    "inset": true,
    "label": "Qatar",
    "value": 36
  },
  {
    "avatar": "/statics/flags/RE.png",
    "continent": false,
    "ind": "RE",
    "inset": true,
    "label": "Réunion",
    "value": 141
  },
  {
    "avatar": "/statics/flags/RO.png",
    "continent": false,
    "ind": "RO",
    "inset": true,
    "label": "Romania",
    "value": 90
  },
  {
    "avatar": "/statics/flags/RU.png",
    "continent": false,
    "ind": "RU",
    "inset": true,
    "label": "Russia",
    "value": 91
  },
  {
    "avatar": "/statics/flags/RW.png",
    "continent": false,
    "ind": "RW",
    "inset": true,
    "label": "Rwanda",
    "value": 142
  },
  {
    "avatar": "/statics/flags/BL.png",
    "continent": false,
    "ind": "BL",
    "inset": true,
    "label": "Saint Barthélemy",
    "value": 200
  },
  {
    "avatar": "/statics/flags/SH.png",
    "continent": false,
    "ind": "SH",
    "inset": true,
    "label": "Saint Helena",
    "value": 143
  },
  {
    "avatar": "/statics/flags/KN.png",
    "continent": false,
    "ind": "KN",
    "inset": true,
    "label": "Saint Kitts and Nevis",
    "value": 201
  },
  {
    "avatar": "/statics/flags/LC.png",
    "continent": false,
    "ind": "LC",
    "inset": true,
    "label": "Saint Lucia",
    "value": 202
  },
  {
    "avatar": "/statics/flags/MF.png",
    "continent": false,
    "ind": "MF",
    "inset": true,
    "label": "Saint Martin",
    "value": 203
  },
  {
    "avatar": "/statics/flags/PM.png",
    "continent": false,
    "ind": "PM",
    "inset": true,
    "label": "Saint Pierre and Miquelon",
    "value": 204
  },
  {
    "avatar": "/statics/flags/VC.png",
    "continent": false,
    "ind": "VC",
    "inset": true,
    "label": "Saint Vincent and the Grenadines",
    "value": 205
  },
  {
    "avatar": "/statics/flags/WS.png",
    "continent": false,
    "ind": "WS",
    "inset": true,
    "label": "Samoa",
    "value": 233
  },
  {
    "avatar": "/statics/flags/SM.png",
    "continent": false,
    "ind": "SM",
    "inset": true,
    "label": "San Marino",
    "value": 92
  },
  {
    "avatar": "/statics/flags/ST.png",
    "continent": false,
    "ind": "ST",
    "inset": true,
    "label": "Sao Tome and Principe",
    "value": 144
  },
  {
    "avatar": "/statics/flags/SA.png",
    "continent": false,
    "ind": "SA",
    "inset": true,
    "label": "Saudi Arabia",
    "value": 37
  },
  {
    "avatar": "/statics/flags/SN.png",
    "continent": false,
    "ind": "SN",
    "inset": true,
    "label": "Senegal",
    "value": 145
  },
  {
    "avatar": "/statics/flags/RS.png",
    "continent": false,
    "ind": "RS",
    "inset": true,
    "label": "Serbia",
    "value": 93
  },
  {
    "avatar": "/statics/flags/SC.png",
    "continent": false,
    "ind": "SC",
    "inset": true,
    "label": "Seychelles",
    "value": 146
  },
  {
    "avatar": "/statics/flags/SL.png",
    "continent": false,
    "ind": "SL",
    "inset": true,
    "label": "Sierra Leone",
    "value": 147
  },
  {
    "avatar": "/statics/flags/SG.png",
    "continent": false,
    "ind": "SG",
    "inset": true,
    "label": "Singapore",
    "value": 38
  },
  {
    "avatar": "/statics/flags/SX.png",
    "continent": false,
    "ind": "SX",
    "inset": true,
    "label": "Sint Maarten",
    "value": 206
  },
  {
    "avatar": "/statics/flags/SK.png",
    "continent": false,
    "ind": "SK",
    "inset": true,
    "label": "Slovakia",
    "value": 94
  },
  {
    "avatar": "/statics/flags/SI.png",
    "continent": false,
    "ind": "SI",
    "inset": true,
    "label": "Slovenia",
    "value": 95
  },
  {
    "avatar": "/statics/flags/SB.png",
    "continent": false,
    "ind": "SB",
    "inset": true,
    "label": "Solomon Islands",
    "value": 234
  },
  {
    "avatar": "/statics/flags/SO.png",
    "continent": false,
    "ind": "SO",
    "inset": true,
    "label": "Somalia",
    "value": 148
  },
  {
    "avatar": "/statics/flags/ZA.png",
    "continent": false,
    "ind": "ZA",
    "inset": true,
    "label": "South Africa",
    "value": 149
  },
  {
    "avatar": "/statics/flags/KR.png",
    "continent": false,
    "ind": "KR",
    "inset": true,
    "label": "South Korea",
    "value": 21
  },
  {
    "avatar": "/statics/flags/SS.png",
    "continent": false,
    "ind": "SS",
    "inset": true,
    "label": "South Sudan",
    "value": 150
  },
  {
    "avatar": "/statics/flags/ES.png",
    "continent": false,
    "ind": "ES",
    "inset": true,
    "label": "Spain",
    "value": 96
  },
  {
    "avatar": "/statics/flags/LK.png",
    "continent": false,
    "ind": "LK",
    "inset": true,
    "label": "Sri Lanka",
    "value": 39
  },
  {
    "avatar": "/statics/flags/SD.png",
    "continent": false,
    "ind": "SD",
    "inset": true,
    "label": "Sudan",
    "value": 151
  },
  {
    "avatar": "/statics/flags/SR.png",
    "continent": false,
    "ind": "SR",
    "inset": true,
    "label": "Suriname",
    "value": 207
  },
  {
    "avatar": "/statics/flags/SJ.png",
    "continent": false,
    "ind": "SJ",
    "inset": true,
    "label": "Svalbard and Jan Mayen",
    "value": 97
  },
  {
    "avatar": "/statics/flags/SZ.png",
    "continent": false,
    "ind": "SZ",
    "inset": true,
    "label": "Swaziland (Eswatini)",
    "value": 152
  },
  {
    "avatar": "/statics/flags/SE.png",
    "continent": false,
    "ind": "SE",
    "inset": true,
    "label": "Sweden",
    "value": 98
  },
  {
    "avatar": "/statics/flags/CH.png",
    "continent": false,
    "ind": "CH",
    "inset": true,
    "label": "Switzerland",
    "value": 99
  },
  {
    "avatar": "/statics/flags/SY.png",
    "continent": false,
    "ind": "SY",
    "inset": true,
    "label": "Syria",
    "value": 40
  },
  {
    "avatar": "/statics/flags/TW.png",
    "continent": false,
    "ind": "TW",
    "inset": true,
    "label": "Taiwan",
    "value": 41
  },
  {
    "avatar": "/statics/flags/TJ.png",
    "continent": false,
    "ind": "TJ",
    "inset": true,
    "label": "Tajikistan",
    "value": 42
  },
  {
    "avatar": "/statics/flags/TZ.png",
    "continent": false,
    "ind": "TZ",
    "inset": true,
    "label": "Tanzania",
    "value": 153
  },
  {
    "avatar": "/statics/flags/TH.png",
    "continent": false,
    "ind": "TH",
    "inset": true,
    "label": "Thailand",
    "value": 43
  },
  {
    "avatar": "/statics/flags/TL.png",
    "continent": false,
    "ind": "TL",
    "inset": true,
    "label": "Timor-Leste",
    "value": 44
  },
  {
    "avatar": "/statics/flags/TG.png",
    "continent": false,
    "ind": "TG",
    "inset": true,
    "label": "Togo",
    "value": 154
  },
  {
    "avatar": "/statics/flags/TK.png",
    "continent": false,
    "ind": "TK",
    "inset": true,
    "label": "Tokelau",
    "value": 235
  },
  {
    "avatar": "/statics/flags/TO.png",
    "continent": false,
    "ind": "TO",
    "inset": true,
    "label": "Tonga",
    "value": 236
  },
  {
    "avatar": "/statics/flags/TT.png",
    "continent": false,
    "ind": "TT",
    "inset": true,
    "label": "Trinidad and Tobago",
    "value": 208
  },
  {
    "avatar": "/statics/flags/TN.png",
    "continent": false,
    "ind": "TN",
    "inset": true,
    "label": "Tunisia",
    "value": 155
  },
  {
    "avatar": "/statics/flags/TR.png",
    "continent": false,
    "ind": "TR",
    "inset": true,
    "label": "Turkey",
    "value": 45
  },
  {
    "avatar": "/statics/flags/TM.png",
    "continent": false,
    "ind": "TM",
    "inset": true,
    "label": "Turkmenistan",
    "value": 46
  },
  {
    "avatar": "/statics/flags/TC.png",
    "continent": false,
    "ind": "TC",
    "inset": true,
    "label": "Turks and Caicos Islands",
    "value": 209
  },
  {
    "avatar": "/statics/flags/TV.png",
    "continent": false,
    "ind": "TV",
    "inset": true,
    "label": "Tuvalu",
    "value": 237
  },
  {
    "avatar": "/statics/flags/UG.png",
    "continent": false,
    "ind": "UG",
    "inset": true,
    "label": "Uganda",
    "value": 156
  },
  {
    "avatar": "/statics/flags/UA.png",
    "continent": false,
    "ind": "UA",
    "inset": true,
    "label": "Ukraine",
    "value": 100
  },
  {
    "avatar": "/statics/flags/AE.png",
    "continent": false,
    "ind": "AE",
    "inset": true,
    "label": "United Arab Emirates",
    "value": 47
  },
  {
    "avatar": "/statics/flags/GB.png",
    "continent": false,
    "ind": "GB",
    "inset": true,
    "label": "United Kingdom",
    "value": 101
  },
  {
    "avatar": "/statics/flags/US.png",
    "continent": false,
    "ind": "US",
    "inset": true,
    "label": "United States",
    "value": 210
  },
  {
    "avatar": "/statics/flags/UY.png",
    "continent": false,
    "ind": "UY",
    "inset": true,
    "label": "Uruguay",
    "value": 211
  },
  {
    "avatar": "/statics/flags/UZ.png",
    "continent": false,
    "ind": "UZ",
    "inset": true,
    "label": "Uzbekistan",
    "value": 48
  },
  {
    "avatar": "/statics/flags/VU.png",
    "continent": false,
    "ind": "VU",
    "inset": true,
    "label": "Vanuatu",
    "value": 238
  },
  {
    "avatar": "/statics/flags/VA.png",
    "continent": false,
    "ind": "VA",
    "inset": true,
    "label": "Vatican City",
    "value": 70
  },
  {
    "avatar": "/statics/flags/VE.png",
    "continent": false,
    "ind": "VE",
    "inset": true,
    "label": "Venezuela",
    "value": 212
  },
  {
    "avatar": "/statics/flags/VN.png",
    "continent": false,
    "ind": "VN",
    "inset": true,
    "label": "Vietnam",
    "value": 49
  },
  {
    "avatar": "/statics/flags/VG.png",
    "continent": false,
    "ind": "VG",
    "inset": true,
    "label": "Virgin Islands (British)",
    "value": 213
  },
  {
    "avatar": "/statics/flags/VI.png",
    "continent": false,
    "ind": "VI",
    "inset": true,
    "label": "Virgin Islands (U.S.)",
    "value": 214
  },
  {
    "avatar": "/statics/flags/WF.png",
    "continent": false,
    "ind": "WF",
    "inset": true,
    "label": "Wallis and Futuna",
    "value": 239
  },
  {
    "avatar": "/statics/flags/EH.png",
    "continent": false,
    "ind": "EH",
    "inset": true,
    "label": "Western Sahara",
    "value": 157
  },
  {
    "avatar": "/statics/flags/YE.png",
    "continent": false,
    "ind": "YE",
    "inset": true,
    "label": "Yemen",
    "value": 50
  },
  {
    "avatar": "/statics/flags/ZM.png",
    "continent": false,
    "ind": "ZM",
    "inset": true,
    "label": "Zambia",
    "value": 158
  },
  {
    "avatar": "/statics/flags/ZW.png",
    "continent": false,
    "ind": "ZW",
    "inset": true,
    "label": "Zimbabwe",
    "value": 159
  }
];

let countriesForSelect = countries.map((a) => {
  delete a.avatar;
  return a;
})/*.sort((a, b) => {
  const A = a.label.slice(0, 1).toUpperCase();
  const B = b.label.slice(0, 1).toUpperCase();
  return ((A > B) ? 1 : (A < B)) ? -1 : 0;
});*/

var contAll= countriesSelect.map(item => ({
    ...item,
    value: item.countryCode || item.code,
  })
);

var cont= contAll.splice(0, 6);
var contAll = contAll.sort(( a, b ) => {
  var nameA=a.label.toLowerCase(), nameB=b.label.toLowerCase();
  if (nameA < nameB) return -1;
  if (nameA > nameB) return 1;
   return 0;
});
cont.push(...contAll);
const countriesWithContinentsForSelect = cont;
/*const countriesWithContinentsForSelect = countriesSelect.map(item => ({
    ...item,
    value: item.countryCode || item.code,
  })
); */


countriesForSelect = countriesForSelect.map(item => ({
  ...item,
  value: item.ind,
}));

const toCountryName = (cca2) => {
  const result = countriesWithContinentsForSelect.filter(country => country.value === cca2);
  if (result.length > 0) {
    return result[0].label;
  }
  return null;
};

const toCCA2 = (countryName) => {
  countryName = decodeURIComponent(countryName);

  let result = countriesSelect.find(country => country.label.toLowerCase() === countryName.toLowerCase());
  if (result) {
    if (result.continent) {
      return countryName;
    }
    return result.countryCode;
  }
  result = countries.find(country => country.label.toLowerCase() === countryName.toLowerCase());
  if (result) {
    return result.ind;
  }
  return null;
};

const toCountryId = (countryName) => {
  const result = countriesSelect.find(country => country.label.toLowerCase() === countryName.toLowerCase());
  if (result && result.continent) {
    return result.value;
  }
  return null;
};
// This tranformation is required in or to align for the offset of continents

const continentToCountries = (countryName) => {
  let result = countriesSelect.find(country => country.label.toLowerCase() === countryName.toLowerCase());
  console.log(result)
  if (result) {
    if (result.continent) {
      return continents[countryName.toLowerCase()];
    }
    return [result.countryCode];
  }
  return null
};
export default countries;
// QSelect component can be populated with countries for select directly
export {
  countriesForSelect,
  countriesWithContinentsForSelect,
  toCountryName,
  toCCA2,
  toCountryId,
  continentToCountries,
};
