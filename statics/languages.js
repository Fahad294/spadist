import l from './languages.json';

const keys = Object.keys(l);
const languagesForSelector = keys.map(k => ({
  label: `${l[k].name} (${l[k].nativeName})`,
  value: k,
}));


export default l;
export { languagesForSelector };
