export default {
  answer: null,
  addTaskOn: false,
  imReady: false,
  todos: [
    {
      heading: 'Packing Checklist:',
      options: [
        {
          title:
                'Get an ultra-light, rolling hard carry-on suitcase and a backpack to get international\ncarry-on allowance easily.',
          done: false,
          heading: 'Light Hand Baggage',
          icon: "block",
          level: "red",
        },
        {
          title:
                'Pack ultra-light – Put out all the clothes you think you need and reduce it by 35%',
          done: false,
          heading: 'Clothes - the lesser, the better',
          icon: "block",
          level: "red",
        },
        {
          title:
                'Weather based apparels &amp; accessories; multipurpose shoes (don’t take 4 pair of shoes\nplease)',
          done: false,
          heading: "Must carry accessories"
          icon: "block",
          level: "red"
        },
        {
          title:
                'Water bottle with built-in filter helps to save your health abroad &amp; planet ��',
          done: false,
          heading: "Water Bottle",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Prepare cellphone: download offline maps, currency &amp; language conversion apps, new\nmusic &amp; turn off data roaming.',
          done: false,
          heading: "Get your smartphones ready",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Pack nice camera, charger, microSD, battery bank, travel universal adapter, epic\nheadphones',
          done: false,
          heading: "Mandatory gadgets",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Medication: Carry basic pain killers, fever, motion sickness pills. Also, mosquito\nrepellent, eye drops (if you are on medicine DON’T forget to bring copies of the\nprescription in carry on)',
          done: false,
          heading: "Basic Medications",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Printed &amp; electronic copies of your passport and visas, emergency contacts written on\npaper in wallet in case you lose your phone.',
          done: false,
          heading: "Essential Paper docs",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Cash: Bring at least 2-3 days spend from home – avoid airport exchanges. Must carry a\ncredit/debit card for emergencies. Carry USD if you cant find XX currency exchange.',
          done: false,
          heading: "Emergency Cash & Card",
          icon: "block",
          level: "red"
        },
        {
          title:
                'For flight: Travel pillow, foldable blanket, eye mask and ear plug\n',
          done: false,
          heading: "Flight-Peace accessories",
          icon: "block",
          level: "red"
        },
      ],
    },
    {
      heading: 'Before leaving house:',
      options: [
        {
          title:
                'Unplug electronic appliances, turn off heater and air conditioner',
          done: false,
          heading: "Check before you leave",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Empty refrigerators of the eatables that will get expired',
          done: false,
          heading: "Get rid of refrigerator stuff",
          icon: "block",
          level: "red"
        },
        {
          title: ' Ask on Facebook to arrange for your pets',
          done: false,
          heading: "Use Facebook for your pets",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Check all windows, doors, water taps &amp; switch off alarm clocks',
          done: false,
          heading: "Secure your house before trip",
          icon: "block",
          level: "red"
        },
        {
          title: 'Store valuables in safer or hidden place of home',
          done: false,
          heading: "Secure your valuables",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Check your flight timings better to set an alarm on mobile',
          done: false,
          heading: "Keep a tab on flight timings",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Inform your bank &amp; confirm if your credit card will work in {{country.name}}',
          done: false,
          heading: "Activate your credit card",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Pre-pay bills, setup email auto responder and Inform neighbors/trusty friend\nabout your travel\n',
          done: false,
          heading: "Stay updated on bills and with neighbors",
          icon: "block",
          level: "red"
          
        },
      ],
    },
    {
      heading: 'Traveling with kids?',
      options: [
        {
          title:
                'Small toys, compact stroller and extra snacks for the flight.',
          done: false,
          heading: "Flight-Friendly snacks and stuff",
          icon: "block",
          level: "red"
        },
        {
          title:
                'Pack change of clothes, diapers &amp; wipes in the carry-on baggage.',
          done: false,
          heading: "Extra set of clothes",
          icon: "block",
          level: "red"
        },
        {
          title: ' Formula, pacifier, extra bottles &amp; nipples.',
          done: false,
          heading: "Accessories for babies",
          icon: "block",
          level: "red"
        },
        {
          title: ' Install kids favorite games in the smartphone / Ipad',
          done: false,
          heading: "Games for Kids onboard",
          icon: "block",
          level: "red"
        },
      ],
    },
  ],
  extraTodos: [],
  todoText: '',
};
